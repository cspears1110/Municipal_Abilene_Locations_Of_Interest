# Isaak Ramirez
# Cole Spears

# IMPORTANT: In order to run this program, you need to have Python 3.6 installed on your computer. You can run the code from a command line or from IDLE.
# Additionally, you need to actually have permission to access the Jixler database.

# First, you should make sure that you have a config.txt file in the same directory as this program. This config.txt file will store your own Jixler-assigned
# User ID and Google Maps API Key. If there is something wrong with your config file, you will be assigned a default user id and API key, however, keep in mind
# that the default API key has limited amount of uses, so please use your own API Key.

# Next, you will be prompted to enter a zip code, or city-state pair. Enter a value such as <79601>, <Abilene, TX>

# Next, you will be prompted to enter an integer that will serve as your query limit value. Enter -1 to insert all returned results.
# If you are worried about over-stuffing the Jixler database with too many entries, you can enter a small number, so that you will 
# not overload the database. Note: If your limit exceeds the returned number of entires, the script will automatically reset your 
# limit to the max. Google API seems to max out at around 20 returned results.

# After this, user input will no longer be required, and the program will enter values into the Jixler database, based on the zip code or city-state that you
# entered. The program enters values for the following fields:
# _id
# END_DATE
# _p_FromUser
# Location
# START_DATE
# accessType
# currentLocation
# jixCategory
# jixMessage
# jixName
# jixRadius
# lattitude
# longitude
# _p_userid
# likeCount
# commentCount

import sys
import math
import requests
import json
import pymongo
from pymongo import MongoClient
from pprint import pprint
import datetime

# The getRadius function takes northeast latitude/longitude coordinates and southwest latitude/longitude coordinates and calculates a radius that
# encompasses the entire area.
def getRadius(lat1, lon1, lat2, lon2):
        deltaLon = math.fabs(lon1 - lon2)
        return deltaLon * 305775 / 3.6 # 3.6 is more of a "correction". It works pretty accurately.
##        return 50;

# The dateConverter function takes the current date/time using the datetime library in Python, then converts the date and time to a format that works for the
# Jixler database
def dateConverter():
        date = str(datetime.datetime.now())
        mdy = date[0:10]
        ti=date[11:]
        a = mdy.split("-")
        b = ti.split(":")
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
        final = ''
        m = ' AM'
        if int(b[0])>=12:
            m = ' PM'
        final+=months[int(a[1])-1]+' '+a[2]+', '+a[0]+' at '+str(int(b[0])%12)+':'+b[1]+m
        return final

# Our main function houses the majority of our code. Check the comments throughout for details about different parts of the program.
def main():
        #This connects to the Jixler databases and uses the "dev" database
        MONGO_HOST = "52.33.219.239"
        MONGO_PORT = 27017
        MONGO_DB = "dev"

        client = MongoClient(MONGO_HOST, MONGO_PORT)
        db = client[MONGO_DB]
        JixInfo = db.JixInfo

        # This program is intended to run alongside a config.txt file. Within the text file, please enter your own Jixler-assigned User ID and your own
        # Google Maps API Key. To find your Jixler-assigned User ID, please access it within the _User Collection in the Jixler "dev" database. If the
        # config.txt file cannot be found, or there is an error within the file, your userID and Google Maps API Key will be assigned to default values.
        try:
            f = open("config.txt")
            check = True
            for line in f:
                b = line.strip().split(":")
                if check:
                    userInput = "_User$"+str(b[1])
                    check = False
                else:
                    API_KEY = str(b[1])
        except:
            print("There was something wrong with your config.txt file.")
            sys.exit();

        # Keep in mind that the default Google Maps API Key has a limit of up to 100 calls/day. To increase this limit, please enter your own Google Maps
        # API Key into the config.txt file.

        # This program searches for any "places of interest" in a given zip code, city, or state
        queryTermString = "places+of+interest+in"
        queryLocationString = input('Please enter a City, State OR Zipcode to search in: ')
        queryString = queryTermString + '+' + queryLocationString.replace(" ", "+")

        # In addition to the given zip code, city, or state that the user enters a limit (integer value). If the value you enter is not an integer,
        # the query limit will be set to the default value of 5. This limit is put in place for certain zip codes that have the possibility to return
        # many different results
        try:
                queryLimit = int(input('Please enter a limit (-1 for max): '))

        except:
                print("You did not enter an integer value for your limit. You will be assigned a default limit of 5.")
                queryLimit = 5

        request = requests.get('https://maps.googleapis.com/maps/api/place/textsearch/json?query=' + queryString + '&key=' + API_KEY)

        # The results of our Google Maps usages get stored in this parsed_json variable. In order to get what we want from this Json string, we must
        # "parse" through it, which means that we are going to select values that line up to different fields that are in the Jixler database.
        parsed_json = json.loads(request.text)

        print('\n', "Number of returned results: ", len(parsed_json['results']), '\n')

        # Here, we print the important values that our Google Maps API search finds
        for i in range(0, len(parsed_json['results'])):
            print('id: \t', parsed_json['results'][i]['id'])
            print('Name: \t', parsed_json['results'][i]['name'])
            print('Lat: \t', parsed_json['results'][i]['geometry']['location']['lat'])
            print('Long: \t',parsed_json['results'][i]['geometry']['location']['lng'])
            print('Calculated Radius: \t', getRadius(parsed_json['results'][i]['geometry']['viewport']['northeast']['lat'], parsed_json['results'][i]['geometry']['viewport']['northeast']['lng'], parsed_json['results'][i]['geometry']['viewport']['southwest']['lat'], parsed_json['results'][i]['geometry']['viewport']['southwest']['lng']))
##            print('NE Lat:\t',parsed_json['results'][i]['geometry']['viewport']['northeast']['lat'])
##            print('NE Long: ',parsed_json['results'][i]['geometry']['viewport']['northeast']['lng'])
##            print('SE Lat:\t',parsed_json['results'][i]['geometry']['viewport']['southwest']['lat'])
##            print('SE Long: ',parsed_json['results'][i]['geometry']['viewport']['southwest']['lng'])
            print('\n')

        if queryLimit == -1:    # reset value to enter all entries
            queryLimit = len(parsed_json['results'])

        limitPosts = []
        duplicateCount = 0
        insertedCount = 0

        # If your returned results exceed your query limit, you will be alerted with an output message, and your results will be reduced accordingly
        if (queryLimit > len(parsed_json['results'])):
            print("Query limit exceeds returned results count. Automatically setting limit to", len(parsed_json['results']), sep=' ')
            queryLimit = len(parsed_json['results'])

        for i in range(0, len(parsed_json['results'])):

            if (insertedCount == queryLimit):
                break

            if (insertedCount ==  len(parsed_json['results'])):
                break

            test = JixInfo.find_one({"_id": parsed_json['results'][i]['id']})

            # It is possible for our program to detect duplicates within the Jixler database. We compare the id's for our searched values to the id's
            # within the Jixler database. If we find a match, the searched value is not added to the Jixler database, and the user is informed.
            if (test):
                print(parsed_json['results'][i]['name'], "already exists at _id:", parsed_json['results'][i]['id'], sep=' ')
                duplicateCount = duplicateCount + 1

            # The majority of our fields are found within our JSON string, but there are a few fields that we can assume. For instance, likeCount and commentCount
            # will always start at 0 for a new Jix.
            elif (insertedCount < queryLimit) :
                dbPost = ({ 
                        "_id": parsed_json['results'][i]['id'],
                        "END_DATE": "Nil",
                        "_p_FromUser": userInput,
                        "Location": str(parsed_json['results'][i]['geometry']['location']['lng']) + "feet," + str(parsed_json['results'][i]['geometry']['location']['lat']) + "feet",
                        "START_DATE": dateConverter(),
                        "accessType": "Public",
                        "currentLocation": [parsed_json['results'][i]['geometry']['location']['lng'],parsed_json['results'][i]['geometry']['location']['lat']],
                        "jixCategory": "JiXwiki",
                        "jixMessage": parsed_json['results'][i]['name'],
                        "jixName": parsed_json['results'][i]['name'],
                        "jixRadius": getRadius(parsed_json['results'][i]['geometry']['viewport']['northeast']['lat'], parsed_json['results'][i]['geometry']['viewport']['northeast']['lng'], parsed_json['results'][i]['geometry']['viewport']['southwest']['lat'], parsed_json['results'][i]['geometry']['viewport']['southwest']['lng']),
                        "lattitude": parsed_json['results'][i]['geometry']['location']['lat'],
                        "longitude": parsed_json['results'][i]['geometry']['location']['lng'],
                        "_p_userid": userInput,
                        "likeCount": 0,
                        "commentCount": 0
                        # Google image (automated) || standard jix wiki image
                })
                limitPosts.append(dbPost)
                insertedCount = insertedCount + 1

        if (limitPosts):
            result = JixInfo.insert_many(limitPosts)

        print("\nFound", duplicateCount, "duplicate entries in database", sep=' ')
        print("Inserted first", insertedCount, "unique post(s) into database", sep=' ')

main()