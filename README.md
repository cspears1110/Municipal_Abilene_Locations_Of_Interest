# Municipality-based JiX Creator
---
#### A Python script written by Isaak Ramirez and Cole Spears
#### to fulfill an undergraduate degree requirement
---
### Usage
The owners of this script give free use for the JiXler Dev team to use and/or modify. The owners assume no ownership in the case of legal questioning. The owners may publish portions of this code for resume/portfolio purposes.

This script runs on Python 3.6, as well as permission to access the JiXler database.

Additionally, this program needs the external libraries *pymongo* and *pprint*. Run the following code to install these
`pip3 install pymongo`
`pip3 install pprint`

The script runs by inserting either a *City, State* (ex. *San Angelo, TX*) or a *Zip Code* (ex. *79601*)
User is then prompted to enter a *queryLimit*, or how many entries should be inserted into the database. The program can also select a value of "-1", which will enter all returned values.

The script then returns a list of locations, pulled from the Google Places API. This list includes all of the necessary elements to insert a basic JiX.

The script then queries the JiXler database to insert only unique ids, thus aleviating the accumulation of identical JiXs. Finally, we enter all unique locations found into the JiXler database.

We imagine this code can be implemented to run for an entire list of *City, States/Zip Codes*, giving the capability to populate many JiXs for entire cities at a time.

#### Some Notes on the Google API
In order to use this API, you must register for a Google Places API key, as well as insert both the API key and a valid JiX User id into the *config.py* script. You can register for an API key [here](https://developers.google.com/places/web-service/)

Currently, the Google throttles the querying of the Google Places API by 100 queries/day (total of 1000, but uses 10 queries / use). There seemed to be the possibility of extending this to 15,000 queries/day. Refer to the Google Places API page [here](https://developers.google.com/maps/pricing-and-plans/#details).

While the original writers of this script could not find much information on the legality of using another company's data for the use of other companies, we wrote this script with the intention that no malcious use of this code will occur. If there are any questions, please refer to Section 2 of the Google API Terms and Conditions: [https://developers.google.com/terms/](https://developers.google.com/terms/)

